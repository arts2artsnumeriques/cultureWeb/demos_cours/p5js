var posX, posY;


function setup() {
  createCanvas(windowWidth, windowHeight);
  stroke(255,0,0);
  strokeWeight(4);
  posX = width/2;
  posY = height/2;
}

function draw() {
  background(255);
  point(posX,posY);
  posX = random(posX - 1, posX + 1);
  posY = random(posX - 1, posX + 1);
}